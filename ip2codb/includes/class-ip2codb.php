<?php

/**
 * The core plugin class.
 */
class Ip2codb {

	protected $loader;
	protected $plugin_name;
	protected $version;

	private $db_sql_date;
	private $db_sql_filepath;

	public function __construct() {

		$this->plugin_name = 'ip2codb';
		$this->version = '1.0.0';

		$this->db_sql_date = '2017-05-21'; // SQL file date included with plugin
		$this->db_sql_filepath = plugin_dir_path( dirname( __FILE__ ) ).'sql/ip2nation.sql'; // SQL file location included with plugin

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies.
	 * Ip2codb_Loader: Orchestrates the hooks of the plugin.
	 * Ip2codb_Admin: Defines all hooks for the admin area.
	 * Ip2codb_Public: Defines all hooks for the public side of the site.
	 */
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-ip2codb-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-ip2codb-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-ip2codb-public.php';

		$this->loader = new Ip2codb_Loader();

	}

	/**
	 * Register admin area hooks.
	 */
	private function define_admin_hooks() {

		$ip2codb_admin = new Ip2codb_Admin( $this->get_plugin_name(), $this->get_version(), $this->db_sql_date, $this->db_sql_filepath );

		$this->loader->add_action( 'admin_enqueue_scripts', $ip2codb_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $ip2codb_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_menu', $ip2codb_admin, 'add_options_page' );

	}

	/**
	 * Register public-facing hooks.
	 */
	private function define_public_hooks() {

		$ip2codb_public = new Ip2codb_Public( $this->get_plugin_name(), $this->get_version() );

	}

	/**
	 * Run the loader.
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
