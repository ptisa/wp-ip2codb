<?php

/**
 * @wordpress-plugin
 * Plugin Name:       IP 2 Country DB
 * Plugin URI:        https://gitlab.com/ptisa/wp-ip2codb
 * Description:       Provides visitor's country of origin based on IP address utilizing Per Gustafsson's ip2nation database.
 * Version:           1.0.0
 * Author:            Peter Tisa
 * Author URI:        http://tisamedia.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ip2codb
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Plugin activation.
 */
function activate_ip2codb() {

	global $wpdb;

	$db_sql_date = '2017-05-21'; // SQL file date included with plugin
	$db_sql_filepath = plugin_dir_path( __FILE__ ).'sql/ip2nation.sql'; // SQL file location included with plugin

	$current_db_date = get_option( 'ip2codb_date' );
	if ( $current_db_date ) {
		$db_version = $current_db_date;
	} else {
		$db_version = $db_sql_date;
	}

	$table_name = 'ip2nation';
	$table_exists = ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name );

	if ( !$table_exists ) {

		// Check if expected file exists before running import
		if ( is_file($db_sql_filepath) ) {

			// Increase PHP timeout - this may take some time
			set_time_limit(300);

			// Prep DB, load, and execute
			$charset_collate = $wpdb->get_charset_collate();
			$sql = file_get_contents( $db_sql_filepath );

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			if ( dbDelta( $sql ) ) {
				if ( !$current_db_date ) {
					add_option( 'ip2codb_date', $db_version );
				}
			}
		}
	}
}

register_activation_hook( __FILE__, 'activate_ip2codb' );

/**
 * Plugin deactivation.
 * Not utilized at this time. Referenced here for future development.
 */
//function deactivate_ip2codb() {
	// do nothing
//}

// Uncomment to add your deactivation routines.
//register_deactivation_hook( __FILE__, 'deactivate_ip2codb' );

/**
 * The core plugin class.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ip2codb.php';

/**
 * Run the plugin.
 */
function run_ip2codb() {

	$plugin = new Ip2codb();
	$plugin->run();

}
run_ip2codb();
