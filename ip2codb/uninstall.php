<?php

/**
 * Fired when the plugin is uninstalled.
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// Delete options
delete_option( 'ip2codb_date' );

// For site options in Multisite
delete_site_option('ip2codb_date');

// Drop database tables. Note: we do not use WP prefixes.
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS ip2nation");
$wpdb->query("DROP TABLE IF EXISTS ip2nationCountries");
