<?php

/**
 * The public-facing functionality of the plugin.
 */
class Ip2codb_Public {

	private $plugin_name;
	private $version;

	public function __construct() {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		global $wpdb;
		$this->wpdb = $wpdb;

	}

	/**
	 * Query the plugin DB for end-user IP match and return location data.
	 * @return array
	 */
	public function get_ip2codb_user_country() {

		$res = array();
		$ip = $_SERVER['REMOTE_ADDR'];

		if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {

			$query = "
				SELECT c.*
				FROM ip2nationCountries c, ip2nation i
				WHERE i.ip < INET_ATON( '".$ip."' )
				AND c.code = i.country
				ORDER BY i.ip DESC
				LIMIT 0,1
			";

			$res = $this->wpdb->get_results($query);
			return $res[0];

		} else {

			return $res; // an empty array

		}
	}

}
