<?php

/**
 * The admin-specific functionality of the plugin.
 */
class Ip2codb_Admin {

	private $plugin_name;
	private $version;
	private $db_sql_date;
	private $db_sql_filepath;

	public function __construct( $plugin_name, $version, $db_sql_date, $db_sql_filepath ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->db_sql_date = $db_sql_date;
		$this->db_sql_filepath = $db_sql_filepath;

		global $wpdb;
		$this->wpdb = $wpdb;

	}

	/**
	 * Register the stylesheet for the admin area.
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ip2codb-admin.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), $this->version, false );

	}

	/**
	 * Register the management page under WP Tools submenu.
	 */
	public function add_options_page() {

		add_management_page(
			'IP 2 Country DB',
			'IP 2 Country DB',
			'install_plugins',
			'ip2codb',
			array( $this, 'render_options_page')
		);

	}

	/**
	 * Render the HTML for plugin management page.
	 */
	public function render_options_page() {

		if ( is_writable( $this->db_sql_filepath ) ) {

			// Process POST actions
			$this->save_options_page();

			// Get some dates for datepicker display
			$current_db_date = get_option('ip2codb_date');
			$option_date_display = date('M d, Y', strtotime($current_db_date));
			$today_date_display = date('M j, Y', time());
			?>

			<div class="wrap">
				<h1><?= esc_html(get_admin_page_title()); ?></h1>
				<p>
					You may update your database with new "ip2nation" database files using the form below.<br />
					Simply upload the new "ip2nation.sql" file downloaded from <a href="http://www.ip2nation.com" target="_blank">www.ip2nation.com</a>.<br />
					You may optionally enter the date the "ip2nation" file was last updated.
				</p>
				<p style="font-weight:bold">
					Current File Date: <?php print $option_date_display; ?>
				</p>
				<form  method="post" enctype="multipart/form-data">
				<table>
				<tr>
					<td><label for="ip2codb_upload_sql">New File:</label></td>
					<td><input type='file' id='ip2codb_upload_sql' name='ip2codb_upload_sql'></input></td>
				</tr>
				<tr>
					<td><label for="ip2codb_date">New Date:</label></td>
					<td><input type="text" id="ip2codb_date" name="ip2codb_date" value="<?php print (!empty($_POST) ? $option_date_display : $today_date_display); ?>" class="ip2codb-datepicker" /></td>
				</tr>
				</table>
				<?php
				wp_nonce_field('ip2codb_update', 'ip2codb_update_nonce');
				submit_button('Submit');
				?>
				</form>
			</div>

		<?php
		} else {
		?>

			<div class="wrap">
				<h1><?= esc_html(get_admin_page_title()); ?></h1>
				<p>
					In order to import updated "ip2nation" files you must set the proper file permissions for the plugin's source file.<br />
					See the README in the plugin's source folder for proper setup instructions.
				</p>
			</div>

		<?php
		}
	}

	/**
	 * Process form submission under plugin management page.
	 */
	public function save_options_page() {

        // File array and labeled as expected - verify nonce
        if (isset($_FILES['ip2codb_upload_sql']) && check_admin_referer('ip2codb_update', 'ip2codb_update_nonce')) {

			// Valid file must be "ip2nation.sql"
			$upload_path = $_FILES['ip2codb_upload_sql']['name'];

			if ( validate_file($upload_path, array('ip2nation.sql')) === 0 ) {

				// Upload file and overwrite existing/previous 'ip2nation.sql' file
				$source = $_FILES['ip2codb_upload_sql']['tmp_name'];
				$destination = $this->db_sql_filepath;
	            $file_status = move_uploaded_file($source, $destination);

				// If a proper date is not entered we just set to current date
				$posted_date = filter_input(INPUT_POST, 'ip2codb_date', FILTER_SANITIZE_STRING);

				if ($this->validate_date($posted_date, 'M d, Y')) {

					$file_date = date('Y-m-d', strtotime($posted_date));

				} else {

					$file_date = date('Y-m-d', time());

				}

	            if ($file_status === TRUE) {

					// Update DB and date option
					$db_status = $this->update_db();

					if ($db_status === TRUE) {

						update_option('ip2codb_date', $file_date);
						print '<div id="message" class="updated"><p>Database updated successfully.</p></div>';

					} else {

						print '<div id="message" class="error"><p>Error encountered updating database. See README for setup instructions.</p></div>';

					}

	            } else {

					print '<div id="message" class="error"><p>Error encountered uploading file. See README for setup instructions.</p></div>';

	            }

			} else {

				print '<div id="message" class="error"><p>Update cancelled. Please choose a "ip2nation.sql" file.</p></div>';

			}
        }
	}

	/**
	 * Updates the ip2nation database tables.
	 * Note: we do not use WP table prefixes as the SQL is provided by a third party.
	 * @return bool
	 */
	function update_db() {

		if (is_file($this->db_sql_filepath)) {

			// Drop any existing tables from DB
			$this->wpdb->query("DROP TABLE IF EXISTS ip2nation");
			$this->wpdb->query("DROP TABLE IF EXISTS ip2nationCountries");

			// Increase PHP timeout - this may take some time
		    set_time_limit(120);

			$charset_collate = $this->wpdb->get_charset_collate();
			$sql = file_get_contents($this->db_sql_filepath);

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			if (dbDelta($sql)) {
				return TRUE;
			}
		}

		return FALSE;
	}

	/**
	 * Validate a date string.
	 * @param  string $date
	 * @param  string $format
	 * @return bool
	 */
	public function validate_date($date, $format='Y-m-d') {

	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;

	}

}
