# IP 2 Country DB

IP 2 Country DB is a simple WordPress plugin that will install the *ip2nation* database tables into your WordPress database.
More information about the ip2nation database is at [www.ip2nation.com](http://www.ip2nation.com/ip2nation)

The plugin will also query the database for each visitor to your website and provide a number of location variables
that you may use in your WordPress templates or other plugins.

When new versions of the *ip2nation* database become available, you may upload them from the WordPress Administration
Tools menu in order to update your WordPress database.

Sample usage that will print all the location variables for a given user:

```php
$ip2codb = new Ip2codb_Public();

$endUserLoc = $ip2codb->get_ip2codb_user_country();

print 'ISO Code 2: ' . $endUserLoc->iso_code_2 . '<br>';
print 'ISO Code 3: ' . $endUserLoc->iso_code_3 . '<br>';
print 'ISO Country: ' . $endUserLoc->iso_country . '<br>';
print 'Country: ' . $endUserLoc->country . '<br>';
print 'Latitude: ' . $endUserLoc->lat . '<br>';
print 'Longitude: ' . $endUserLoc->lon . '<br>';
```

## Installation

1. Upload the `ip2codb` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin via the 'Plugins' menu in WordPress.
3. Plugin settings are located in "Tools" > "IP 2 Country DB".

## Database Updates

Some additional configuration may be required in order to apply updates to the *ip2nation* tables from the plugin's settings page.
If your updates to the *ip2nation* database fail to upload. Be sure and check the following requirements:

* The new *ip2nation* SQL file should be named "ip2nation.sql".
* Your PHP configuration should allow file uploads of at least 5 megabytes.
* The folder/file at `/wp-content/plugins/ip2codb/sql/ip2nation.sql` needs to be writable by the webserver
or system user under which WordPress is running.
